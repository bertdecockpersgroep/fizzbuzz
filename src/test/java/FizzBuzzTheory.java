import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;

@RunWith(Theories.class)
public class FizzBuzzTheory {

    public static final int THREE = 3;
    public static final int FIVE = 5;
    public static final int SEVEN = 7;

    public static @DataPoints int[] numbers = IntStream.range(-1000, 1000).toArray(); //these get passed to each of the theories

    @Theory
    public void numbers_not_devidable_by_three_five_or_seven_result_in_themselves(int number) {
        assumeThat(number % THREE).isNotEqualTo(0); //theory will be skipped if assumption does not pass
        assumeThat(number % FIVE).isNotEqualTo(0);
        assumeThat(number % SEVEN).isNotEqualTo(0);
        assertThat(fizzbuzz(number)).isEqualTo(String.valueOf(number));
    }

    @Theory
    public void numbers_devidable_by_three_result_in_fuzz(int number) {
        assumeThat(number % THREE).isEqualTo(0);
        assumeThat(number % FIVE).isNotEqualTo(0);
        assumeThat(number % SEVEN).isNotEqualTo(0);
        assertThat(fizzbuzz(number)).isEqualTo("fizz");
    }

    @Theory
    public void numbers_devidable_by_five_result_in_buzz(int number) {
        assumeThat(number % FIVE).isEqualTo(0);
        assumeThat(number % THREE).isNotEqualTo(0);
        assumeThat(number % SEVEN).isNotEqualTo(0);
        assertThat(fizzbuzz(number)).isEqualTo("buzz");
    }

    @Theory
    public void numbers_devidable_by_seven_result_in_bar(int number) {
        assumeThat(number % SEVEN).isEqualTo(0);
        assumeThat(number % THREE).isNotEqualTo(0);
        assumeThat(number % FIVE).isNotEqualTo(0);
        assertThat(fizzbuzz(number)).isEqualTo("bar");
    }

    @Theory
    public void numbers_devidable_by_three_and_five_result_in_fizzbuzz(int number) {
        assumeThat(number % THREE).isEqualTo(0);
        assumeThat(number % FIVE).isEqualTo(0);
        assumeThat(number % SEVEN).isNotEqualTo(0);
        assertThat(fizzbuzz(number)).isEqualTo("fizzbuzz");
    }

    @Theory
    public void numbers_devidable_by_three_and_seven_result_in_fizzbar(int number) {
        assumeThat(number % THREE).isEqualTo(0);
        assumeThat(number % SEVEN).isEqualTo(0);
        assumeThat(number % FIVE).isNotEqualTo(0);
        assertThat(fizzbuzz(number)).isEqualTo("fizzbar");
    }

    @Theory
    public void numbers_devidable_by_five_and_seven_result_in_buzzbar(int number) {
        assumeThat(number % FIVE).isEqualTo(0);
        assumeThat(number % SEVEN).isEqualTo(0);
        assumeThat(number % THREE).isNotEqualTo(0);
        assertThat(fizzbuzz(number)).isEqualTo("buzzbar");
    }

    @Theory
    public void numbers_devidable_by_three_five_and_seven_result_in_fizzbuzzbar(int number) {
        assumeThat(number % THREE).isEqualTo(0);
        assumeThat(number % FIVE).isEqualTo(0);
        assumeThat(number % SEVEN).isEqualTo(0);
        assertThat(fizzbuzz(number)).isEqualTo("fizzbuzzbar");
    }

    private String fizzbuzz(int number) {
        String result = "";
        if (number % THREE == 0) {
            result += "fizz";
        }
        if (number % FIVE == 0) {
            result += "buzz";
        }
        if (number % SEVEN == 0) {
            result += "bar";
        }
        if ("".equals(result)) {
            result = String.valueOf(number);
        }

        return result;
    }

}
