import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class FizzBuzzTest {

    public static final int THREE = 3;
    public static final int FIVE = 5;
    public static final int SEVEN = 7;
    private final int input;
    private final String expected;

    public FizzBuzzTest(int input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameters(name = "{0} -> {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {1, "1"},
                {2, "2"},
                {4, "4"},

                {THREE, "fizz"},
                {2* THREE, "fizz"},

                {FIVE, "buzz"},
                {2* FIVE, "buzz"},

                {SEVEN, "bar"},
                {2* SEVEN, "bar"},

                {THREE * FIVE, "fizzbuzz"},

                {THREE * SEVEN, "fizzbar"},
                {2* THREE * SEVEN, "fizzbar"},

                {FIVE * SEVEN, "buzzbar"},

                {THREE * FIVE * SEVEN, "fizzbuzzbar"},
        });
    }

    @Test
    public void testFizzbuzz() {
        assertThat(fizzbuzz(input)).isEqualTo(expected);
    }

    @Test
    public void testFizzbuzz2() {
        assertThat(fizzbuzz2(input)).isEqualTo(expected);
    }

    private String fizzbuzz(int number) {
        if (number % 105 == 0) return "fizzbuzzbar";
        if (number % 35 == 0) return "buzzbar";
        if (number % 21 == 0) return "fizzbar";
        if (number % 15 == 0) return "fizzbuzz";
        if (number % THREE == 0) return "fizz";
        if (number % FIVE == 0) return "buzz";
        if (number % SEVEN == 0) return "bar";

        return String.valueOf(number);
    }

    private String fizzbuzz2(int number) {
        String result = "";
        if (number % THREE == 0) {
            result += "fizz";
        }
        if (number % FIVE == 0) {
            result += "buzz";
        }
        if (number % SEVEN == 0) {
            result += "bar";
        }
        if ("".equals(result)) {
            result = String.valueOf(number);
        }

        return result;
    }

}
